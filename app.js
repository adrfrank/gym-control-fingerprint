const { app, BrowserWindow, ipcMain } = require('electron');
const { exec } = require('child_process');
const url = require("url");
const path = require("path");

let appWindow;

function createWindow() {
    appWindow = new BrowserWindow({
        width: 1200,
        height: 800,
        webPreferences: {
            nodeIntegration: true,
            nodeIntegrationInWorker: true,
            contextIsolation: false
        }
    });
  
    // Electron Build Path
    appWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `/dist/index.html`),
        protocol: "file:",
        slashes: true
      })
    );
  
    // Initialize the DevTools.
    //appWindow.webContents.openDevTools();
  
    appWindow.on('closed', function () {
      appWindow = null
    });

    appWindow.removeMenu();


    ipcMain.on('read-devices', (event, args) => {
        console.log("read-devices event")
        console.log("Args:", args);
        try{
            var cp = exec(`fprintd-verify user_${args}`);
            var msg = ""
            cp.stdout.on("data", (data) => {
                msg +=  data.toString();
            })
            cp.on("exit", code => {
                console.log("Exit code", code);
                console.log("Message", msg)
                const regex = /verify-match \(done\)/;
                if(regex.exec(msg)){
                    console.log("Correct verification");
                    event.reply('readed-devices', true);
                } else {
                    console.log("Failed verification");
                    event.reply('readed-devices', false);
                }
            });
        }catch(err){
            console.error(err);
            event.reply('readed-devices', false);
        }
    });

    ipcMain.on("enroll-member", (event, args) => {
        console.log("enroll-member event");
        console.log("Args:", args);
        try{
            var cp = exec(`fprintd-enroll user_${args}`);
            var msg = "";
            cp.stdout.on("data", (data) => {
                msg +=  data.toString();
            })
            cp.on("exit", code => {
                console.log("Exit code", code);
                console.log("Message", msg)
                const regex = /enroll-completed/;
                if(regex.exec(msg)){
                    console.log("Correct enrollment");
                    event.reply('enroll-member-finished', true);
                } else {
                    console.log("Failed enrollment");
                    event.reply('enroll-member-finished', false);
                }
            });
        }catch(err){
            console.error(err);
            event.reply('enroll-member-finished', false);
        }
    });

    ipcMain.on("verify-enrollment", (event, args) =>{
        console.log("verify-enrollment event");
        console.log("Args: ", args);
        try{
            var childP = exec(`fprintd-list user_${args}`);
            var msg = "";
            childP.stdout.on("data", data => {
                msg += data.toString();
            })
            childP.on("exit", code => {
                console.log("Exit code", code);
                console.log("Message", msg);
                const regex = /\- #\d/;
                if(regex.exec(msg)){
                    console.log("User has at least 1 finger enrolled");
                    event.reply("verified-enrollment", true);
                }else { 
                    console.log("Verify enrollment failed");
                    event.reply("verified-enrollment", false);
                }
            })
        } catch(err) {
            console.error(err);
            event.reply("verify-enrollment-fail", err);
        }
    });
    
}
app.whenReady().then( () => {
    createWindow();

    app.on('activate',  () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    });
});

  
// Close when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS specific close process
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
  





