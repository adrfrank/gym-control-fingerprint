import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FingerprintControlComponent } from './fingerprint-control.component';

describe('FingerprintControlComponent', () => {
  let component: FingerprintControlComponent;
  let fixture: ComponentFixture<FingerprintControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FingerprintControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FingerprintControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
