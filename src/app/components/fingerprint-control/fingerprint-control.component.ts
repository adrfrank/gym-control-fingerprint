import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DeviceSettings } from 'src/app/models/device.settings';
import { FingerprintService } from 'src/app/services/fingerprint.service';
import { IpcService } from 'src/app/services/ipc.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { MembersApiService } from 'src/app/services/members-api.service';
import { SoundServiceService } from 'src/app/services/sound-service.service';

//https://dev.to/michaeljota/integrating-an-angular-cli-application-with-electron---the-ipc-4m18

@Component({
  selector: 'app-fingerprint-control',
  templateUrl: './fingerprint-control.component.html',
  styleUrls: ['./fingerprint-control.component.css']
})
export class FingerprintControlComponent implements OnInit  {
  isIntroducingCode = true;
  isVerifyingFingerprint = false;
  isFingerprintOK=false;
  isFingerprintFail=false;
  memberCode= null;
  message = ""
  member= null;
  deviceSettings: DeviceSettings;
  timeoutId: NodeJS.Timeout;

  constructor(
    private readonly _ipc: IpcService, 
    private readonly fingerprint: FingerprintService, 
    private memberApi: MembersApiService,
    private snackBar: MatSnackBar,
    private storageService: LocalStorageService,
    private router: Router,
    private soundService: SoundServiceService) { 

  }

  ngOnInit(): void {
      this.deviceSettings = this.storageService.getItem("deviceSettings") as DeviceSettings;
      if(!this.deviceSettings){
        console.warn("No device settings, going to setup...");
        this.router.navigate(["device-setup"])
      }
  }

  reset(){
    this.isVerifyingFingerprint = false;
    this.isFingerprintFail = false;
    this.isFingerprintOK = false;
    this.memberCode = null;
    this.message = "";
    this.member = null;
  }

  async applyFilter(event: any) {
    console.log("Calling API to fetch user", );
    this.isIntroducingCode = false;
    this.memberApi.getMember(this.memberCode).subscribe(async member => {
      this.member = member;
      this.isVerifyingFingerprint = true;
      this.message = "Ingrese su huella digital"
      if(this.deviceSettings?.enableFingerprint){
        let userHasFingerprints = await this.fingerprint.verifyEnrollment(this.member.id);
        if(!userHasFingerprints) this.router.navigate(["enroll", this.member.id]);
        let result = await this.fingerprint.validateFingerprint(this.member.id) as boolean;
        this.handleFingerPrintResult(result);
      }else{
        this.handleFingerPrintResult(true);//true because fingerprints are not enabled
      }
      
    }, err => {
      console.error(err);
      this.message = "Usuario no encontrado"
      this.snackBar.open("Usuario no encontrado", "Cerrar", { duration: 3000 });
      this.soundService.playErrorBeep();
      this.timeoutReset(2000);
    });
    
  }

  timeoutReset(ms){
    clearTimeout(this.timeoutId);

    this.timeoutId = setTimeout(() => {
      this.reset();
    }, ms);
    
  }

  handleFingerPrintResult(result: boolean){
    this.isFingerprintOK = result;
    this.isFingerprintFail = !result;
    if(result){
      this.memberApi.logVisit(this.member.id).subscribe(visit => {
        this.message = "";
        this.isIntroducingCode = false;
      }, err => {
        this.message = "Error registrando la visita";
      });
      this.memberCode = null;
      this.soundService.playBeep();
    }else{
      console.log("Fail")
      this.message = "Hay un problema con tu huella";
      this.soundService.playErrorBeep();
    }
    this.timeoutReset(2000);
  }

}
