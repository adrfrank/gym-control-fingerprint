import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollFingerprintComponent } from './enroll-fingerprint.component';

describe('EnrollFingerprintComponent', () => {
  let component: EnrollFingerprintComponent;
  let fixture: ComponentFixture<EnrollFingerprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnrollFingerprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollFingerprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
