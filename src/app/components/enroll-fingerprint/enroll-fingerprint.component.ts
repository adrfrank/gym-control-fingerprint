import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IpcService } from 'src/app/services/ipc.service';
import { MembersApiService } from 'src/app/services/members-api.service';

@Component({
  selector: 'app-enroll-fingerprint',
  templateUrl: './enroll-fingerprint.component.html',
  styleUrls: ['./enroll-fingerprint.component.css']
})
export class EnrollFingerprintComponent implements OnInit {

  memberId: number;
  message: string;
  member: any;
  capturingFingerprint: boolean;

  constructor(private memberService: MembersApiService, private ipc: IpcService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.capturingFingerprint = false;
    this.ipc.on("enroll-member-finished", (result) => {
      this.capturingFingerprint = false;
      if(!result){
        this.message = "Error registrando la huella del usuario."
      }else {
        this.message = `Huella del miembro ${this.member.name} ${this.member.last_name} registradas correctamente`;
        this.memberId =  null;
      }
    });
    this.route.paramMap.subscribe( paramMap => {
      var memberId = paramMap.get("memberId");
      if(memberId)
        this.memberId = parseInt(memberId);
    });
  }

  verifyAndEnroll(){
    console.log("Verify and Enroll member");
    this.message = "";
    this.memberService.getMember(this.memberId).subscribe(member =>{
      this.member = member;
      this.message =  `Agregando huella para el miembro ${member.name} ${member.last_name}`
      this.capturingFingerprint = true;
      this.ipc.send("enroll-member", this.memberId);
    },error =>{
      this.message = `No se encontro el miembro ${this.memberId}`;
    });
  }
}
