import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DeviceSettings } from 'src/app/models/device.settings';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { map } from 'rxjs/operators'
import { MatSnackBar } from '@angular/material/snack-bar';

const SettingsKey = "deviceSettings";

@Component({
  selector: 'app-device-setup',
  templateUrl: './device-setup.component.html',
  styleUrls: ['./device-setup.component.css']
})
export class DeviceSetupComponent implements OnInit {

  deviceSettings: DeviceSettings;
  message: String;

  constructor(private storageService: LocalStorageService, private http: HttpClient, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    
    this.deviceSettings = this.storageService.getItem(SettingsKey) as DeviceSettings;
    if (!this.deviceSettings){
      console.log("No settigns creating empty values")
      this.deviceSettings =  new DeviceSettings();
      console.log("Saving Settings");
      this.storageService.setItem(SettingsKey, this.deviceSettings);

    }
    console.log("Loading Settings")
  }

  saveData(){
    console.log("Saving Settings");
    this.storageService.setItem(SettingsKey, this.deviceSettings);
    this._snackBar.open("Guardado", "Cerrar", { duration: 3000 });
  }

  connect(){
    console.log("Connecting with server");
    const body = {
      username : this.deviceSettings.deviceUser,
      password : this.deviceSettings.devicePassword
    }

    this.http.post<any>(`${this.deviceSettings.serverAddress}/api/users/authenticate`, body).subscribe(res => {
      console.log("resp", res);
      this._snackBar.open("Conexión exitosa", "Cerrar", { duration: 3000 });
      this.storageService.setItem("currentUser", res);
      return res;
    }, err => {
      console.log("Error",err);
      this._snackBar.open("Erorr conectando con el servidor", "Cerrar", { duration: 3000 });
    });
    
    
  }

}
