import { TestBed } from '@angular/core/testing';

import { FakeFingerprintService } from './fake-fingerprint.service';

describe('FakeFingerprintService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FakeFingerprintService = TestBed.get(FakeFingerprintService);
    expect(service).toBeTruthy();
  });
});
