import { Injectable } from '@angular/core';
import { Howl } from 'howler'

@Injectable({
  providedIn: 'root'
})
export class SoundServiceService {
  private beepSound: Howl;
  private errorBeepSound: Howl;

  constructor() { 
    this.beepSound = new Howl({
      src: ['/assets/sounds/beep-07a.mp3']
    });
    this.errorBeepSound = new Howl({
      src: ['/assets/sounds/beep-03.mp3']
    });
  }

  playBeep(){
    this.beepSound.play();
  }

  playErrorBeep(){
    this.errorBeepSound.play();
  }
}
