import { Injectable } from '@angular/core';
import { IpcService } from './ipc.service';

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  constructor(private readonly _ipc: IpcService) { 

  }

  validateFingerprint(id) :  Promise<Boolean> {
    var b = new Promise<Boolean>( resolver => {
      this._ipc.send("read-devices", id)
      this._ipc.on("readed-devices", (result, args) => {
        resolver(args);
      })
    })
  
    return b;
  }

  verifyEnrollment(id) : Promise<boolean> {
    var b = new Promise<boolean>( (resolver, reject) => {
      this._ipc.send("verify-enrollment", id);
      this._ipc.on("verified-enrollment", (result, args) => {
        resolver(args);
      });
      this._ipc.on("verify-enrollment-fail", (result, args) => {
        reject(args);
      });
    });
    return b;
  }

}
