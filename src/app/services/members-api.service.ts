import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from './local-storage.service';
import { DeviceSettings } from '../models/device.settings';

@Injectable({
  providedIn: 'root'
})
export class MembersApiService {
  
  constructor(private httpClient: HttpClient, private storageService: LocalStorageService) { 
  }

  get baseUrl() {
    return (this.storageService.getItem("deviceSettings") as DeviceSettings).serverAddress;
  }

  getMember(id){
    return this.httpClient.get<any>(this.baseUrl+'/api/members/'+id);
  }

  logVisit(id, action = "ENTRADA"){
    const body = {
      member_id :  id,
      entry_date : new Date(),
      action : action
    }
    return this.httpClient.post<any>(`${this.baseUrl}/api/members/${id}/visits`, body);
  }

}
