import { Injectable } from '@angular/core';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class FakeFingerprintService {

  constructor() { }

  counter = 0;
  validateFingerprint() :  Promise<Boolean> {
    this.counter ++;
    let p = new Promise<Boolean>( resolve =>  setTimeout( ()=> { resolve(this.counter%2 == 0)}, 1000 ));
    return p;
  }
}
