
export class DeviceSettings {
    devicePassword: String = "";
    deviceUser: String = "";
    serverAddress: String = "";
    enableFingerprint: Boolean = true;
}