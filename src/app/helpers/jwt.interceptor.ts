import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from '../services/local-storage.service';
import { DeviceSettings } from '../models/device.settings';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private storageService: LocalStorageService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //add auth header with jwt if use is logged in and request is to the api url
    const currentuser =  this.storageService.getItem("currentUser");
    const deviceSettings = this.storageService.getItem("deviceSettings") as DeviceSettings;
    const isLoggedIn = currentuser && currentuser.token;
    const isApiUrl = request.url.startsWith(deviceSettings.serverAddress.toString());
    if(isLoggedIn && isApiUrl){
      request = request.clone({
        setHeaders: {Authorization: `Bearer ${currentuser.token}`}
      });
    }

    return next.handle(request);
  }
}
