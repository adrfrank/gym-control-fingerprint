import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceSetupComponent } from './components/device-setup/device-setup.component';
import { EnrollFingerprintComponent } from './components/enroll-fingerprint/enroll-fingerprint.component';
import { FingerprintControlComponent } from './components/fingerprint-control/fingerprint-control.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  { path: 'fingerprint-control', component: FingerprintControlComponent },
  { path: 'device-setup', component: DeviceSetupComponent},
  { path: 'enroll/:memberId', component: EnrollFingerprintComponent},
  { path: '',   redirectTo: 'fingerprint-control', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }