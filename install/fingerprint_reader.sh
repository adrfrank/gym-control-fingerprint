echo "Install finger print reader"
sudo apt install fprintd

echo "Creating udev rule for allowing current user to access reader"

echo "ATTRS{idVendor}==\"05ba\", ATTRS{idProduct}==\"000a\", MODE=\"0664\", GROUP=\"fingerprint\"" \
    | sudo tee /etc/udev/rules.d/99-fingerprint.rules

echo "Creating finger print rule and add $USER"
sudo groupadd fingerprint
sudo gpasswd -a $USER fingerprint

echo "All fingerprint group to write fingerprints"
sudo chmod -R g+rw /var/lib/fprint
sudo chgrp -R fingerprint /var/lib/fprint

echo "Creating a rule for avoid requesting access when enrolling a new fingerprint"
echo "polkit.addRule(function (action, subject) { \
  if (action.id == \"net.reactivated.fprint.device.setusername\") {\
    polkit.log(\"action=\" + action);\
    polkit.log(\"subject=\" + subject);\
    return polkit.Result.YES ;\
  }\
})" | sudo tee /usr/share/polkit-1/rules.d/50-net.reactivated.fprint.device.enroll.rules

sudo cp ./install/50-com.gymadmin.override-fingerprint.pkla /etc/polkit-1/localauthority/50-local.d/